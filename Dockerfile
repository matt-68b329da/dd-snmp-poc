FROM amazonlinux:latest AS snmp-host
RUN yum install -y -q deltarpm
RUN yum install -y -q https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum update -y -q
RUN yum makecache fast -y -q
RUN yum install -y -q net-snmp net-snmp-utils
RUN net-snmp-config-x86_64 --default-mibs

ENTRYPOINT /bin/bash -c "snmptrapd -CdfLo --disableAuthorization=yes & snmpd -a -f -Lo"
