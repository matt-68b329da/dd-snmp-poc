dd-snmp-poc
===========

#### description

A `docker-compose` simulating monitoring several `SNMP` devices spread across different subnets, with one `datadog-agent` per subnet acting as a `SNMP` Manager

![snmp-demo.png](./diagrams/snmp-demo.png)

#### usage

###### start

    # docker-compose up --build -d

###### stop

    # docker-compose down -t 1 --remove-orphans

#### custom dashboard

The dashboard in the following screenshot is available in this repository under `dashboard/dd-snmp-dashboard.json`

![https://share.getcloudapp.com/12urzbnN/download/Image%202020-10-26%20at%207.49.53%20AM.png](https://share.getcloudapp.com/12urzbnN/download/Image%202020-10-26%20at%207.49.53%20AM.png)

#### video demo

The following video shows a simulated outage of a `SNMP` device, alerting and then recovery :

- [link to video demo webpage](https://share.getcloudapp.com/2Nuj79nX)
- [link to video demo mp4 download](https://share.getcloudapp.com/2Nuj79nX/download/Screen%20Recording%202020-10-26%20at%2007.52%20AM.mp4)

![in-line video demo mp4](https://share.getcloudapp.com/2Nuj79nX/download/Screen%20Recording%202020-10-26%20at%2007.52%20AM.mp4)

#### SNMP MIBs enabled

- DISMAN-EVENT-MIB
- EtherLike-MIB
- HOST-RESOURCES-MIB
- IF-MIB
- IP-FORWARD-MIB
- IP-MIB
- IPV6-MIB
- MTA-MIB
- NOTIFICATION-LOG-MIB
- RMON-MIB
- SCTP-MIB
- SNMPv2-MIB
- SNMPv2-SMI
- TCP-MIB
- UDP-MIB

		# snmpwalk -v2c -cpublic localhost | cut -d':' -f1 | sort | uniq | grep [A-Z]

#### reference & useful links

- [https://docs.datadoghq.com/integrations/snmp/](https://docs.datadoghq.com/integrations/snmp/)
- [https://datadoghq.dev/integrations-core/tutorials/snmp/introduction/](https://datadoghq.dev/integrations-core/tutorials/snmp/introduction/)
- [https://docs.datadoghq.com/network_performance_monitoring/devices/setup/?tab=snmpv2](https://docs.datadoghq.com/network_performance_monitoring/devices/setup/?tab=snmpv2)
- [https://www.datadoghq.com/blog/monitor-snmp-with-datadog/](https://www.datadoghq.com/blog/monitor-snmp-with-datadog/)
- [http://www.net-snmp.org/](http://www.net-snmp.org/)
- [http://www.net-snmp.org/docs/mibs/](http://www.net-snmp.org/docs/mibs/)
- [http://www.mibdepot.com/index.shtml](http://www.mibdepot.com/index.shtml)
